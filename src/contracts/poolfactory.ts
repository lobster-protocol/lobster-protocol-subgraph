import { PoolFactory } from "../../generated/PoolFactory/PoolFactory";
import {
    Address,
  } from '@graphprotocol/graph-ts';
import { managerAddressesLobster } from "../utils/constant";

export class PoolFactoryContract {

    contractAddress: Address;
    contract: PoolFactory

    constructor(contractAddress: Address) {
        this.contractAddress = contractAddress;
        this.contract = PoolFactory.bind(contractAddress);
    }

    public getAllAdresses(): Array<Address> {
        const address = Address.fromString(managerAddressesLobster);
        let allVaults = this.contract.try_getManagedPools(address);
        if (!allVaults.reverted) {
            return allVaults.value;
        } else {
            return new Array<Address>();
        }
    }
}