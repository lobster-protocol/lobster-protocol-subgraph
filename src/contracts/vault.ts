// types
import {
    Address,
    BigInt,
    BigDecimal
} from '@graphprotocol/graph-ts';
// generated
import { PoolLogic } from '../../generated/templates/PoolLogic/PoolLogic';
import { PoolManagerLogic } from '../../generated/templates/PoolLogic/PoolManagerLogic';
//utils
import { convertTokenToDecimal } from '../utils/decimals';
// constants
import { ZERO_BI } from '../utils/constant';


// class contract
export class DhedgeVaultContract {

    contractAddress: Address;
    contract: PoolLogic

    constructor(contractAddress: Address) {
        this.contractAddress = contractAddress;
        this.contract = PoolLogic.bind(contractAddress);
    }

    public fetchVaultManagerContractAddress(): Address {
        return this.contract.poolManagerLogic();
    }

    public fetchVaultName(): string {
        let tryVaultName = this.contract.try_name();
        let name = ""
        if (!tryVaultName.reverted) {
            name = tryVaultName.value
        } 
        return name
    }

    public fetchVaultTotalSupply(): BigInt {
        return this.contract.totalSupply();
    }

    public fetchVaultReceiptTokenPrice(vaultReceiptTokenDecimals: BigInt): BigDecimal{
        let tryReceiptTokenPrice = this.contract.try_tokenPrice();
        let receiptTokenPrice = ZERO_BI
        if (!tryReceiptTokenPrice.reverted) {
            receiptTokenPrice = tryReceiptTokenPrice.value
        }
        return convertTokenToDecimal(receiptTokenPrice, vaultReceiptTokenDecimals);
    }
}


// class contract
export class DhedgeVaultManagerContract {

    contractAddress: Address;
    contract: PoolManagerLogic
    
    constructor(contractAddress: Address) {
        this.contractAddress = contractAddress;
        this.contract = PoolManagerLogic.bind(contractAddress);
    }

    public fetchManagerAddress(): Address {
        return this.contract.manager();
    }

    public fetchManagerName(): string {
        return this.contract.managerName();
    }
}