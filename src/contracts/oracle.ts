//types
import {
    Address,
    BigInt,
    BigDecimal
  } from '@graphprotocol/graph-ts';
// generated
import { Oracle } from '../../generated/templates/PoolLogic/Oracle';
// utils
import { convertTokenToDecimal } from '../utils/decimals';


// class contract
export class ChainlinkOracleContract {

    contractAddress: Address;
    contract: Oracle
    
    constructor(contractAddress: Address) {
        this.contractAddress = contractAddress;
        this.contract = Oracle.bind(contractAddress);
    }

    public fetchActualQuote(): BigDecimal {
        let actualQuote = this.contract.latestAnswer();
        return convertTokenToDecimal(actualQuote, BigInt.fromI32(8));
    }
}
