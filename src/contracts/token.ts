//types
import {
    Address,
    BigInt
  } from '@graphprotocol/graph-ts';
// generated
import { ERC20 } from '../../generated/templates/PoolLogic/ERC20';


// class contract
export class ER20Contract {

    contractAddress: Address;
    contract: ERC20

    constructor(contractAddress: Address) {
        this.contractAddress = contractAddress;
        this.contract = ERC20.bind(contractAddress);
    }

    public fetchTokenDecimals(): BigInt {
        let decimalValue = NaN;
        let decimalResult = this.contract.try_decimals();
        if (!decimalResult.reverted) {
          decimalValue = decimalResult.value;
        }
        return BigInt.fromI32(decimalValue as i32);
    }

    public fetchBalanceOf(investorAddress: Address): BigInt {
        return this.contract.balanceOf(investorAddress);
    }
}