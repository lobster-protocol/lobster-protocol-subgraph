import { Address, ethereum} from "@graphprotocol/graph-ts";
// events
import {
  Deposit as DepositEvent,
  Withdrawal as WithdrawalEvent,
} from "../../generated/templates/PoolLogic/PoolLogic"
// schemas
import {
  InvestorOperation,
  Vault,
} from "../../generated/schema"
// utils
import { convertTokenToDecimal, convertToBigInt } from '../utils/decimals';
import { dhedgeFactoryAddress } from "../utils/constant";
// helpers
import { instantiateVault } from "../helpers/vault";
import { instantiateInvestor, instantiateInvestorBalance } from '../helpers/investor';
// enums
import { OperationType } from "../enums/operation";
// services
import { fetchChainlinkQuote } from '../services/quote';
// contracts
import { ER20Contract } from "../contracts/token";
import { DhedgeVaultContract } from "../contracts/vault";
import { PoolFactoryContract } from "../contracts/poolfactory";



export function handleDeposit(event: DepositEvent): void {
  let entity = new InvestorOperation(
    event.transaction.hash.concatI32(event.logIndex.toI32())
  );
  let vaultAddress = event.params.fundAddress;
  let investorAddress =  event.transaction.from;
  let operationType =  OperationType.DEPOSIT;

  let vault = instantiateVault(vaultAddress, event.params.assetDeposited);
  let investor = instantiateInvestor(investorAddress);

  let receiptTokenContract = new ER20Contract(vaultAddress);
  let hedgedTokenContract = new ER20Contract(vaultAddress);
  let dhedgeVaultContract = new DhedgeVaultContract(vaultAddress);

  let vaultHedgedTokenPrice = fetchChainlinkQuote(vault.hedgedTokenAddress);
  let receiptTokenDecimals = receiptTokenContract.fetchTokenDecimals();
  let hedgedTokenDecimals = hedgedTokenContract.fetchTokenDecimals();
  let receiptTokenPrice = dhedgeVaultContract.fetchVaultReceiptTokenPrice(receiptTokenDecimals);

  let hedgedTokenAmountDeposited = convertTokenToDecimal(event.params.amountDeposited, hedgedTokenDecimals);
  let operationReceiptTokenAmount = convertTokenToDecimal(event.params.fundTokensReceived, receiptTokenDecimals);
  let operationUSDAmount = convertTokenToDecimal(event.params.valueDeposited, receiptTokenDecimals);
  let newInvestorReceiptTokenBalance = convertTokenToDecimal(receiptTokenContract.fetchBalanceOf(investorAddress), receiptTokenDecimals);
  let investorBalance = instantiateInvestorBalance(investorAddress, 
                                                   newInvestorReceiptTokenBalance, 
                                                   vaultHedgedTokenPrice, 
                                                   hedgedTokenDecimals, 
                                                   receiptTokenPrice, 
                                                   vaultAddress, 
                                                   hedgedTokenAmountDeposited, 
                                                   operationType);

  entity.operationType = operationType;
  entity.hedgedTokenPrice = vaultHedgedTokenPrice;
  entity.receiptTokenPrice = receiptTokenPrice;
  entity.operationUSDAmount = operationUSDAmount;
  entity.operationHedgedTokenAmount = hedgedTokenAmountDeposited;
  entity.operationReceiptTokenAmount = operationReceiptTokenAmount;
  entity.vault = vault.id;
  entity.investor = investor.id;
  entity.investorReceiptTokensBalance = investorBalance.receiptTokensBalance;
  entity.investorTotalHedgedTokenDeposited = investorBalance.totalHedgedTokenDeposited;
  entity.timeStamp = event.params.time;
  entity.transactionHash = event.transaction.hash;
  entity.save();
}


export function handleWithdrawal(event: WithdrawalEvent): void {
  let entity = new InvestorOperation(
    event.transaction.hash.concatI32(event.logIndex.toI32())
  );
  let vaultAddress = event.params.fundAddress;
  let investorAddress =  event.transaction.from;
  let operationType =  OperationType.WITHDRAWAL;

  let vault = instantiateVault(vaultAddress);
  let investor = instantiateInvestor(investorAddress);

  let receiptTokenContract = new ER20Contract(vaultAddress);
  let dhedgeVaultContract = new DhedgeVaultContract(vaultAddress);

  let vaultHedgedTokenPrice = fetchChainlinkQuote(vault.hedgedTokenAddress);
  let receiptTokenDecimals = vault.receiptTokenDecimals;
  let hedgedTokenDecimals = vault.hedgedTokenDecimals;
  let receiptTokenPrice = dhedgeVaultContract.fetchVaultReceiptTokenPrice(receiptTokenDecimals);
  
  let operationUSDAmount = convertTokenToDecimal(event.params.valueWithdrawn, receiptTokenDecimals);
  let withdrawnHedgedTokenAmount = convertTokenToDecimal(convertToBigInt(operationUSDAmount.div(vaultHedgedTokenPrice), hedgedTokenDecimals), hedgedTokenDecimals);
  let operationReceiptTokenAmount = convertTokenToDecimal(event.params.fundTokensWithdrawn, receiptTokenDecimals);
  let newInvestorReceiptTokenBalance = convertTokenToDecimal(receiptTokenContract.fetchBalanceOf(investorAddress), receiptTokenDecimals);
  let investorBalance = instantiateInvestorBalance(investorAddress, 
                                                   newInvestorReceiptTokenBalance, 
                                                   vaultHedgedTokenPrice,
                                                   hedgedTokenDecimals, 
                                                   receiptTokenPrice, 
                                                   vaultAddress, 
                                                   withdrawnHedgedTokenAmount, 
                                                   operationType);
  
  entity.operationType = operationType;
  entity.hedgedTokenPrice = vaultHedgedTokenPrice;
  entity.receiptTokenPrice = receiptTokenPrice;
  entity.operationUSDAmount = operationUSDAmount;
  entity.operationHedgedTokenAmount = withdrawnHedgedTokenAmount;
  entity.operationReceiptTokenAmount = operationReceiptTokenAmount;
  entity.vault = vault.id;
  entity.investor = investor.id;
  entity.investorReceiptTokensBalance = investorBalance.receiptTokensBalance;
  entity.investorTotalHedgedTokenDeposited = investorBalance.totalHedgedTokenDeposited;
  entity.timeStamp = event.params.time;
  entity.transactionHash = event.transaction.hash;
  entity.save();
}

export function refreshVaultDataPrices(block: ethereum.Block): void {
  const factoryContract = new PoolFactoryContract(Address.fromString(dhedgeFactoryAddress));
  let allVaults = factoryContract.getAllAdresses();
  if (allVaults.length != 0)   {
    allVaults.forEach((vaultAddress) => {
      let vaultEntity = Vault.load(vaultAddress.toHexString());
      if (vaultEntity !== null) {
        let vaultContract = new DhedgeVaultContract(vaultAddress);
        let receiptTokenContract = new ER20Contract(vaultAddress);
        let receiptTokenDecimals = receiptTokenContract.fetchTokenDecimals();
        let receiptTokenPrice = vaultContract.fetchVaultReceiptTokenPrice(receiptTokenDecimals);
        let receiptTokenTotalSupply = vaultContract.fetchVaultTotalSupply();
        let hedgedTokenPrice = fetchChainlinkQuote(vaultEntity.hedgedTokenAddress);
        vaultEntity.receiptTokenTotalSupply = receiptTokenTotalSupply;
        vaultEntity.receiptTokenPrice = receiptTokenPrice;
        vaultEntity.hedgedTokenPrice = hedgedTokenPrice;
        vaultEntity.save();
      }
    });
  }
}