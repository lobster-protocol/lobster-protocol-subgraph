// templates
import { PoolLogic as PoolLogicTemplate } from '../../generated/templates'
// schemas
import {
    Manager
} from '../../generated/schema'
// events
import {
    FundCreated as FundCreatedEvent
} from '../../generated/PoolFactory/PoolFactory'

import { managerAddressesLobster } from '../utils/constant';


export function handleFundCreated(event: FundCreatedEvent): void {
    const managerAddressLobster = managerAddressesLobster
    
    let managerAddress = event.params.manager.toHexString();
    if (managerAddress != managerAddressLobster) {
        return;
    }

    let manager = Manager.load(managerAddress);
    if (!manager) {
        manager = new Manager(managerAddress);
        manager.managerAddress = event.params.manager;
    }
    manager.save();
    PoolLogicTemplate.create(event.params.fundAddress);
}