import { NFT,Investor } from "../../generated/schema";
import { TransferSingle } from "../../generated/NFTContract/erc1155Abi";
import { BigInt } from "@graphprotocol/graph-ts";
import { TransferBatch } from "../../generated/NFTContract/erc1155Abi";

export function handleTransferSingle(event: TransferSingle): void {
  let tokenId = event.params.id.toString();
  let to = event.params.to;
  let from = event.params.from;
  let nftTo = NFT.load(tokenId + "-" + to.toHex());
  let value = event.params.value;

  if (from.toHex() != "0x0000000000000000000000000000000000000000") {
    let nftFromId = tokenId + "-" + from.toHex();
    let nftFrom = NFT.load(nftFromId);

    if (nftFrom) {
      nftFrom.balance = nftFrom.balance.minus(value);
      if (nftFrom.balance.equals(BigInt.fromI32(0))) {
        nftFrom.balance = BigInt.fromI32(0);
        }
      nftFrom.save();
    }
  }
  if (!nftTo) {
    nftTo = new NFT(tokenId + "-" + to.toHex());
    nftTo.owner = to;
    nftTo.tokenId = event.params.id;
    nftTo.balance = event.params.value;
    let investor = Investor.load(to.toHex());
    if (!investor) {
      investor = new Investor(to.toHex());
      investor.investorAddress = to;
    }
    nftTo.investor = investor.id;
    investor.save();
  }  else {
    nftTo.balance = nftTo.balance.plus(value);
  }
  nftTo.save();
}

export function handleTransferBatch(event: TransferBatch): void {
  let from = event.params.from;
  let to = event.params.to;
  let ids = event.params.ids;
  let values = event.params.values;
  for (let i = 0; i < ids.length; i++) {
    let tokenId = ids[i].toString();
    let value = values[i];
    let nftTo = NFT.load(tokenId + "-" + to.toHex());

    if (from.toHex() != "0x0000000000000000000000000000000000000000") {
      let nftFromId = tokenId + "-" + from.toHex();
      let nftFrom = NFT.load(nftFromId);

      if (nftFrom) {
        nftFrom.balance = nftFrom.balance.minus(value);
        if (nftFrom.balance.equals(BigInt.fromI32(0))) {
          nftFrom.balance = BigInt.fromI32(0);
        }
        nftFrom.save();
      }
    }
    if (!nftTo) {
      nftTo = new NFT(tokenId + "-" + to.toHex());
      nftTo.owner = to;
      nftTo.tokenId = ids[i];
      nftTo.balance = values[i];
      let investor = Investor.load(to.toHex());
      if (!investor) {
        investor = new Investor(to.toHex());
        investor.investorAddress = to;
      }
      nftTo.investor = investor.id;
      investor.save();
    } else {
      nftTo.balance = nftTo.balance.plus(value);
    }
    nftTo.save();
  }
}
