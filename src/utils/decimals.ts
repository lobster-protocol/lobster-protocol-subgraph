import {
  BigInt,
  BigDecimal
} from '@graphprotocol/graph-ts';
import { ZERO_BI, ONE_BI } from './constant';


function exponentToBigDecimal(decimals: BigInt): BigDecimal {
  let bd = BigDecimal.fromString('1');
  for (let i = ZERO_BI; i.lt(decimals as BigInt); i = i.plus(ONE_BI)) {
    bd = bd.times(BigDecimal.fromString('10'));
  }
  return bd;
}

function convertBigDecimalsToBigInt(bigDecimal: BigDecimal): BigInt {
  return BigInt.fromString(bigDecimal.toString().split('.')[0]);
}


export function convertTokenToDecimal(
  tokenAmount: BigInt,
  exchangeDecimals: BigInt
): BigDecimal {
  if (exchangeDecimals == ZERO_BI) {
    return tokenAmount.toBigDecimal();
  }
  return tokenAmount.toBigDecimal().div(exponentToBigDecimal(exchangeDecimals));
}


export function convertToBigInt(
  tokenAmount: BigDecimal,
  exchangeDecimals: BigInt
): BigInt {
  return convertBigDecimalsToBigInt(tokenAmount.times(exponentToBigDecimal(exchangeDecimals)));
}