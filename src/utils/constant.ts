import {
    BigInt,
    BigDecimal,
    Address
  } from '@graphprotocol/graph-ts';


const dev = '0xfd97f7a0cd1eff880c8eec2011a4685e8ca86370';
const staging =  '0x03d4c3f62ca46bc25b2cac59aa2047a2156af5a5';
const production =  '0x6ebb1b5be9bc93858f71714ed03f67bf237473cb';


export let ZERO_BD = BigDecimal.fromString("0");
export let ZERO_BI = BigInt.fromI32(0);
export let ONE_BI = BigInt.fromI32(1);
export let BASIC_DECIMALS_BI = BigInt.fromI32(18);

export const oracles: Map<string, Address> = new Map<string, Address>();
export const dhedgeFactoryAddress = "0xffFb5fB14606EB3a548C113026355020dDF27535";
oracles.set("0x82af49447d8a07e3bd95bd0d56f35241523fbab1", Address.fromString("0x639fe6ab55c921f74e7fac1ee960c0b6293ba612"));
oracles.set("0x2f2a2543b76a4166549f7aab2e75bef0aefc5b0f", Address.fromString("0x6ce185860a4963106506c203335a2910413708e9"));

export const managerAddressesLobster = production;
