export namespace OperationType {
    export const DEPOSIT = "deposit";
    export const WITHDRAWAL = "withdrawal";
}