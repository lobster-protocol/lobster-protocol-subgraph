// types
import {
    Address,
    BigDecimal,
    Bytes
} from '@graphprotocol/graph-ts';
// contracts
import { ChainlinkOracleContract } from '../contracts/oracle';
// utils
import { oracles } from '../utils/constant';


export function fetchChainlinkQuote(
    tokenAddress: Bytes
  ): BigDecimal {
    let tokenOracleAddress = oracles.get(Address.fromBytes(tokenAddress).toHexString());
    let oracleContract = new ChainlinkOracleContract(tokenOracleAddress);
    let tokenQuote = oracleContract.fetchActualQuote();
    return tokenQuote;
}