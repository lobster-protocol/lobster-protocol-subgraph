// types
import {
    BigDecimal
} from '@graphprotocol/graph-ts';
// enums
import { OperationType } from '../enums/operation';
// utils
import { ZERO_BD } from '../utils/constant';


export function computeInvestorNewDepositValue(
    lastInvestorReceiptTokenBalance: BigDecimal,
    lastInvestorTotalHedgedTokenDeposited: BigDecimal,
    vaultHedgedTokenPrice: BigDecimal,
    receiptTokenPrice: BigDecimal,
    operationHedgedTokenAmount: BigDecimal,
    operationType: string
  ): BigDecimal {
    let newInvestorTotalHedgedTokenDeposited = ZERO_BD;
    if (operationType == OperationType.DEPOSIT) {
        newInvestorTotalHedgedTokenDeposited = lastInvestorTotalHedgedTokenDeposited.plus(operationHedgedTokenAmount);
    } else if (operationType == OperationType.WITHDRAWAL) {
        if (lastInvestorTotalHedgedTokenDeposited.equals(ZERO_BD)) {
            return ZERO_BD
        }
        let profit = lastInvestorReceiptTokenBalance.times(receiptTokenPrice)
                        .div(lastInvestorTotalHedgedTokenDeposited.times(vaultHedgedTokenPrice));
        
        newInvestorTotalHedgedTokenDeposited = lastInvestorTotalHedgedTokenDeposited.minus(operationHedgedTokenAmount.div(profit));
    }

    return newInvestorTotalHedgedTokenDeposited;
}