// types
import { Address, BigDecimal, BigInt} from '@graphprotocol/graph-ts';
// schemas
import {
  Investor, InvestorBalance
} from "../../generated/schema"
// services
import { computeInvestorNewDepositValue } from '../services/investor';
// utils
import { convertToBigInt, convertTokenToDecimal } from '../utils/decimals';
import { ZERO_BD } from '../utils/constant';


export function instantiateInvestor(
    investorAddress: Address,
  ): Investor {
    let investor = Investor.load(investorAddress.toHexString());
    if (!investor) {
        investor = new Investor(investorAddress.toHexString());
        investor.investorAddress = investorAddress;
    }
    investor.save();
    return investor as Investor;
}


export function instantiateInvestorBalance(
    investorAddress: Address,
    newInvestorReceiptTokenBalance: BigDecimal,
    vaultHedgedTokenPrice: BigDecimal,
    hedgedTokenDecimals: BigInt,
    receiptTokenPrice: BigDecimal,
    vaultAddress: Address,
    operationHedgedTokenAmount: BigDecimal,
    operationType: string
  ): InvestorBalance {
    let investorBalance = InvestorBalance.load(investorAddress.toHexString().concat(vaultAddress.toHexString()));
    let newInvestorTotalHedgedTokenDeposited = newInvestorReceiptTokenBalance.times(receiptTokenPrice).div(vaultHedgedTokenPrice);
    
    if (!investorBalance) {
        investorBalance = new InvestorBalance(investorAddress.toHexString().concat(vaultAddress.toHexString()));
        investorBalance.investor = investorAddress.toHexString();
        investorBalance.vault = vaultAddress.toHexString();
    } else {
        let lastInvestorReceiptTokenBalance = investorBalance.receiptTokensBalance;
        let lastInvestorTotalHedgedTokenDeposited = investorBalance.totalHedgedTokenDeposited;
        newInvestorTotalHedgedTokenDeposited = computeInvestorNewDepositValue(lastInvestorReceiptTokenBalance, lastInvestorTotalHedgedTokenDeposited, vaultHedgedTokenPrice, receiptTokenPrice, operationHedgedTokenAmount, operationType);
    }
    if (newInvestorReceiptTokenBalance.equals(ZERO_BD)) {
      newInvestorTotalHedgedTokenDeposited = ZERO_BD
    }
    investorBalance.receiptTokensBalance = newInvestorReceiptTokenBalance;
    investorBalance.totalHedgedTokenDeposited = convertTokenToDecimal(convertToBigInt(newInvestorTotalHedgedTokenDeposited, hedgedTokenDecimals), hedgedTokenDecimals);
    investorBalance.save();
    return investorBalance as InvestorBalance;
}

