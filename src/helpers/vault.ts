// types
import {
    Address,
} from '@graphprotocol/graph-ts';
// schemas 
import { Vault } from '../../generated/schema';
// contracts
import { DhedgeVaultContract } from '../contracts/vault'
import { DhedgeVaultManagerContract } from '../contracts/vault';
import { ER20Contract } from '../contracts/token';


export function instantiateVault(
    vaultAddress: Address,
    depositedAsset: Address = Address.fromString('0x82af49447d8a07e3bd95bd0d56f35241523fbab1')
  ): Vault {
    let vault = Vault.load(vaultAddress.toHexString());
    let vaultContract = new DhedgeVaultContract(vaultAddress);
    let vaultManagerContractAddress = vaultContract.fetchVaultManagerContractAddress();
    let managerContract = new DhedgeVaultManagerContract(vaultManagerContractAddress);
    let receiptTokenContract = new ER20Contract(vaultAddress);
    let hedgedTokenContract = new ER20Contract(depositedAsset);
    
    if (!vault) {
      vault = new Vault(vaultAddress.toHexString());
      vault.vaultAddress = vaultAddress;
      vault.receiptTokenDecimals = receiptTokenContract.fetchTokenDecimals();
      vault.hedgedTokenAddress = depositedAsset;
      vault.hedgedTokenDecimals = hedgedTokenContract.fetchTokenDecimals();
      vault.receiptTokenAddress = vaultAddress;
    }
    
    vault.vaultName = vaultContract.fetchVaultName();
    vault.managerAddress = managerContract.fetchManagerAddress();
    vault.managerName = managerContract.fetchManagerName();
    vault.receiptTokenTotalSupply = vaultContract.fetchVaultTotalSupply();
    vault.save();

    return vault as Vault;
}