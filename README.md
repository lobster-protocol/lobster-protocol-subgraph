# Lobster Protocol Subgraph Documentation

## Overview

The Lobster Protocol Subgraph indexes and processes data from the Lobster Protocol smart contracts. It is integral to applications like the Lobster Protocol Dashboard, providing reliable and up-to-date information for users and developers.

## Functionality

The subgraph is deployed on the **Arbitrum One** network and updates every 5,000 blocks (approximately every 10 minutes). It retrieves key data such as prices for the hedged token and the receipt token, ensuring accurate and timely insights.

## Interaction Guide

### Accessing the Subgraph

You can interact with the subgraph through the **[Lobster Graph Explorer](https://thegraph.com/explorer/subgraphs/4t4mCziV4kTvaSzzNWsTCae9UQKHwQh6EjyR4CYw43rn?view=Query&chain=arbitrum-one)**. The subgraph uses **GraphQL**, allowing flexible queries for retrieving data.

#### Example Query
Here is a sample GraphQL query to fetch details about the first five created vaults:

```graphql
{
  vaults(first: 5) {
    id
    vaultName
    vaultAddress
    managerAddress
  }
}
```

You can explore all available query options by clicking the **GraphQL** button in the Lobster Graph Explorer interface.

---

## Updating the Subgraph

### Development, Local, and Staging Environments

To deploy updates in non-production environments:

1. Clone the subgraph repository.
2. Create a new subgraph on The Graph’s platform.
3. Follow The Graph's [Quick Start Tutorial](https://thegraph.com/docs/developer/quick-start#3-build-and-deploy-your-subgraph) to build and deploy the subgraph within the repository.

### Production Environment

To deploy updates in the production environment, follow the same tutorial linked above. **Note:** Production deployments require access to the Lobster Wallet Multisig.

---

## Architecture and Structure

The subgraph is organized as follows:

```
src/
├── contracts/
├── enums/
├── handlers/
├── helpers/
├── services/
├── utils/
abis/
```

### Directory Descriptions

- **contracts**: Smart contract ABIs used by the subgraph.
- **enums**: Enumerations utilized throughout the subgraph.
- **handlers**: Event handlers for processing contract events.
- **helpers**: Utility functions supporting handlers.
- **services**: Reusable service modules for business logic.
- **utils**: General utility functions for shared use.
- **abis**: ABI files defining contract interfaces.

---

## Key Components

### GraphQL Schema

The subgraph schema includes the following entities:

- **Vault**: Information about individual vaults.
- **Investor**: Data on investors within the protocol.
- **InvestorOperation**: Details of investor transactions.
- **InvestorBalance**: Records of investor balances.
- **NFT**: Metadata and ownership of NFTs.
- **Manager**: Details about vault managers.

---

### Configuration

The subgraph configuration is defined in the **subgraph.yaml** file. The current version is **1.2.0**.

#### PoolFactory Contract

The **PoolFactory** contract is responsible for creating vaults and is configured as follows:

- **Production**:
  - Starting Block: **171097151**
  - Address: **0xffFb5fB14606EB3a548C113026355020dDF27535**

- **Development**:
  - Starting Block: **170000675**
  - Address: **0xfd97f7a0cd1eff880c8eec2011a4685e8ca86370**

- **Staging**:
  - Starting Block: **170000675**
  - Address: **0x03d4c3f62ca46bc25b2cac59aa2047a2156af5a5**

#### NFTContract

The **NFTContract** is used across all environments:

- Starting Block: **225176919**
- Address: **0xae1CE49e0465fB0f30aD08bCBbb15aa6e4D93bC0**

**Event Handlers**:
- **TransferSingle**: Tracks individual NFT transfers.
- **TransferBatch**: Tracks batch NFT transfers.

#### BlockHandler

The **BlockHandler** updates the subgraph every 1,000 blocks. It ensures up-to-date data on vaults, including hedged token and receipt token prices.

#### PoolLogic

The **PoolLogic** module processes vault-related events:

- **Deposit**: Logs deposits into vaults.
- **Withdrawal**: Logs withdrawals from vaults.

These handlers synchronize user and vault data in real time.